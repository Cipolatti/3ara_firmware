/**
 * Library for all the calculus needed using forward kinematics
 * Forward kinematics = knowing the articulations positions (angles) you can obtain the X, Y and Z position of the end point of the robot arm
 * referenced to the origin of coordinates
 */
#include "main.h"
#include <math.h>

/**
 * Enum to select forward or inverse kinematics from PC
 */
typedef enum {
	FORWARD, INVERSE
} Kinematics;

Kinematics ChosenKinematics;
/**
 * Define of the links
 */
#define L1	240								//L1 is the distance between the origin and articulation 2 (mm)
#define L2	230								//is the length of the arm (mm)
#define L3 	200								//length of the forearm (mm)

/**
 * @brief	Function use in Forward kinematics
 * @param
 * @return
 * @Returns an array of length 3. In position 0 is place X coord, pos 2 is Y corrd and pos 3, Z coord.
 */
void getEffectorPosition(double theta1, double theta2, double theta3, double *buffer);

/**
 * @Function use in Inverse Kinematics
 * @Returns an array of length 3. In position 0 is theta1, in pos 2 is theta2 and in pos 3 is theta3.
 */
void getArticulationAngles(uint32_t X, uint32_t Y, uint32_t Z, double *buffer);
