#include <stdbool.h>
#include "main.h"
#include "tim.h"
#include "freertos.h"
#include "IRencoder.h"
#include "cmsis_os.h"

//Defines
#define VEL_FOR_ZERO_POSITION 	600					//Duty cycle to move the three motors at a low speed for home positioning

#define MAX_PID_OUTPUT 			1000						//100% - Max value that can be the duty for the motor (1000)
#define MIN_PID_OUTPUT 			-1000						//0% - Minor value that can be the duty for the motor (-1000)

extern float actual_angle_1, actual_angle_2, actual_angle_3;

/**
 * @brief	Function for moving motor 1 right direction - Shoulder
 * @param	Duty cicle to set the speed
 */
void motor1MoveRight(uint32_t duty);

/**
 * @brief	Function for moving motor 1 left direction - Shoulder
 * @param	Duty cicle to set the speed
 */
void motor1MoveLeft(uint32_t duty);

/**
 * @brief	Function for moving motor 2 right direction - Arm
 * @param	Duty cicle to set the speed
 */
void motor2MoveRight(uint32_t duty);

/**
 * @brief	Function for moving motor 2 left direction - Arm
 * @param	Duty cicle to set the speed
 */
void motor2MoveLeft(uint32_t duty);

/**
 * @brief	Function for moving motor 3 right direction - Forearm
 * @param	Duty cicle to set the speed
 */
void motor3MoveRight(uint32_t duty);

/**
 * @brief	Function for moving motor 3 leftright direction - Forearm
 * @param	Duty cicle to set the speed
 */
void motor3MoveLeft(uint32_t duty);


/**
 * @brief	This function is called everytime the robot arm is turn on. Takes the robot arm to it's home position, so the encoders
 * 			can start reading angles from that position.
 */
void moveToZeroPos();

/**
 * @brief	Moves articulation 1 to it's zero position
 */
void move1ToZeroPos();

/**
 * @brief	Moves articulation 2 to it's zero position
 */
void move2ToZeroPos();

/**
 * @brief	Moves articulation 3 to it's zero position
 */
void move3ToZeroPos();

float pidMotor1(float setpoint, float current_position);

float pidMotor2(float setpoint, float current_position);

float pidMotor3(float setpoint, float current_position);
