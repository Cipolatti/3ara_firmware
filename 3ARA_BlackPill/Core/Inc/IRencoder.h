
#include "main.h"
#include "tim.h"
#include "stdint.h"
#include <math.h>

#define ENCODER_ACQUISITION_TIME 7					//Time in milliseconds required for actualization of the angle
#define ENCODER_ACQUISITION_TIME_S	0.021				//Time in seconds


/**
 * @brief	Gets velocity of the motor in RPM
 * @param	Number of the encoder you want to measure
 * @return	RPM of the chosen motor
 */
float getRPM(uint16_t encoder);

/**
 * @brief	Gets the actual angle of the motor in degrees
 * @param 	Number of the encoder you want to measure
 * @return	Angle in degrees of the chosen motor
 */
float getAngle1();

float getAngle2();

float getAngle3();


