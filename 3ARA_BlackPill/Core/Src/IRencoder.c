
#include "IRencoder.h"

float slots = 1800;										//Number of slots of the optical encoder
extern float encoder_1, encoder_2, encoder_3;

float getRPM(uint16_t encoder){
	float rpm;
	switch(encoder){
		case 1:
			encoder_1 = __HAL_TIM_GetCounter(&htim4);
			rpm = (encoder_1 / 0.05) * 60 / slots;
			break;
		case 2:
			//encoder_2 = __HAL_TIM_GetCounter(&htim8);
			//rpm = (encoder_2 / 0.05) * 60 / slots;
			break;
		case 3:
			encoder_3 = __HAL_TIM_GetCounter(&htim3);
			rpm = (encoder_3 / 0.05) * 60 / slots;
			break;
	}
	return rpm;
}

float getAngle1(){
	float angle=0;
	encoder_1 = __HAL_TIM_GetCounter(&htim4);
	angle = encoder_1 * 0.0625f;
	return floor(angle);
}

float getAngle2(){
	float angle=0;
	encoder_2 = __HAL_TIM_GetCounter(&htim8);
	angle = encoder_2 * 0.05f;
	return floor(angle);
}

float getAngle3(){
	float angle=0;
	encoder_3 = __HAL_TIM_GetCounter(&htim3);
	angle = encoder_3 * 0.09f; //0.0625
	return floor(angle);
}
