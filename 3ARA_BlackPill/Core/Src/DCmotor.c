
#include "DCmotor.h"

uint8_t firstCalcule_1 = 1, firstCalcule_2 = 1, firstCalcule_3 = 1;
extern float ki_1, ki_2, ki_3, kp_1,kp_2, kp_3, kd_1, kd_2, kd_3;

void motor1MoveRight(uint32_t duty){
	HAL_GPIO_WritePin(MOTOR_1_L_GPIO_Port, MOTOR_1_L_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(MOTOR_1_R_GPIO_Port, MOTOR_1_R_Pin, GPIO_PIN_RESET);
	htim2.Instance -> CCR2 = duty;
}


void motor1MoveLeft(uint32_t duty){
	HAL_GPIO_WritePin(MOTOR_1_R_GPIO_Port, MOTOR_1_R_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(MOTOR_1_L_GPIO_Port, MOTOR_1_L_Pin, GPIO_PIN_RESET);
	htim2.Instance -> CCR2 = duty;
}

void motor2MoveRight(uint32_t duty){
	HAL_GPIO_WritePin(MOTOR_2_L_GPIO_Port, MOTOR_2_L_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(MOTOR_2_R_GPIO_Port, MOTOR_2_R_Pin, GPIO_PIN_SET);
	htim2.Instance -> CCR1 = duty;
}

void motor2MoveLeft(uint32_t duty){
	HAL_GPIO_WritePin(MOTOR_2_R_GPIO_Port, MOTOR_2_R_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(MOTOR_2_L_GPIO_Port, MOTOR_2_L_Pin, GPIO_PIN_SET);
	htim2.Instance -> CCR1 = duty;
}

void motor3MoveRight(uint32_t duty){
	HAL_GPIO_WritePin(MOTOR_3_L_GPIO_Port, MOTOR_3_L_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(MOTOR_3_R_GPIO_Port, MOTOR_3_R_Pin, GPIO_PIN_SET);
	htim2.Instance -> CCR3 = duty;
}

void motor3MoveLeft(uint32_t duty){
	HAL_GPIO_WritePin(MOTOR_3_R_GPIO_Port, MOTOR_3_R_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(MOTOR_3_L_GPIO_Port, MOTOR_3_L_Pin, GPIO_PIN_SET);
	htim2.Instance -> CCR3 = duty;
}

void moveToZeroPos(){
	/*
	motor1MoveLeft(VEL_FOR_ZERO_POSITION);
	while(HAL_GPIO_ReadPin(END_STOP_2_GPIO_Port, END_STOP_2_Pin) != GPIO_PIN_RESET);

	motor2MoveLeft(VEL_FOR_ZERO_POSITION);
	while(HAL_GPIO_ReadPin(END_STOP_2_GPIO_Port, END_STOP_2_Pin) != GPIO_PIN_RESET);
	*/
	motor3MoveLeft(VEL_FOR_ZERO_POSITION);
	while(HAL_GPIO_ReadPin(END_STOP_3_GPIO_Port, END_STOP_3_Pin) != GPIO_PIN_RESET);
}

void move1ToZeroPos(){
	motor1MoveLeft(VEL_FOR_ZERO_POSITION);
	while(HAL_GPIO_ReadPin(END_STOP_1_GPIO_Port, END_STOP_1_Pin) != GPIO_PIN_RESET){
		osDelay(2);
	}
	htim2.Instance -> CCR2 = 0;
	__HAL_TIM_SetCounter(&htim4, 0);
	actual_angle_1 = 0;
	// Reset encoder count - Position zero reached

}

void move2ToZeroPos(){
	motor2MoveRight(VEL_FOR_ZERO_POSITION);
	while(HAL_GPIO_ReadPin(END_STOP_2_GPIO_Port, END_STOP_2_Pin) != GPIO_PIN_RESET){
		osDelay(2);
	}
	htim2.Instance -> CCR1 = 0;
	// Reset encoder count - Position zero reached
	__HAL_TIM_SetCounter(&htim8, 0);
	actual_angle_2 = 0;
}

void move3ToZeroPos(){
	motor3MoveLeft(300);
	while(HAL_GPIO_ReadPin(END_STOP_3_GPIO_Port, END_STOP_3_Pin) != GPIO_PIN_RESET){
		osDelay(10);
	}
	htim2.Instance -> CCR3 = 0;
	// Reset encoder count - Position zero reached
	__HAL_TIM_SetCounter(&htim3, 0);
	actual_angle_3 = 0;
}

float pidMotor1(float setpoint, float current_position){
	float error=0, derivative=0, output=0;
	static float old_error_1 = 0, integral = 0;
	if (firstCalcule_1 == 1) {
		integral = 0;
		derivative = 0;
		old_error_1 = 0;
		firstCalcule_1 = 0;
	}

	error = setpoint - current_position;
	integral = integral + (error * ENCODER_ACQUISITION_TIME_S);
	if(integral >= 5) integral = 5;
	else if(integral < -5) integral = -5;
	derivative = (error - old_error_1) / ENCODER_ACQUISITION_TIME_S;
	output = (kp_1 * error) + (kd_1 * derivative) + (ki_1 * integral);
	if (output > MAX_PID_OUTPUT)
		output = MAX_PID_OUTPUT;
	else if (output < MIN_PID_OUTPUT)
		output = MIN_PID_OUTPUT;
	old_error_1 = error;
	return output;
}

float pidMotor2(float setpoint, float current_position){
	float error = 0, derivative = 0, output = 0;
	//extern float error, derivative, integral;
	//float output;
	static float old_error_2 = 0, integral = 0;
	if (firstCalcule_2 == 1) {
		derivative = 0;
		old_error_2 = 0;
		firstCalcule_2 = 0;
	}
	error = setpoint - current_position;
	integral = integral + (error * ENCODER_ACQUISITION_TIME_S);
	if(integral >= 5) integral = 5;
	else if(integral < -5) integral = -5;
	derivative = (error - old_error_2) / ENCODER_ACQUISITION_TIME_S;
	output = (kp_2 * error) + (kd_2 * derivative) + (ki_2 * integral);
	if (output > MAX_PID_OUTPUT)
		output = MAX_PID_OUTPUT;
	else if (output < MIN_PID_OUTPUT)
		output = MIN_PID_OUTPUT;
	old_error_2 = error;
	return output;
}

float pidMotor3(float setpoint, float current_position){
	float error = 0, derivative = 0, output=0;
	static float old_error_3 = 0, integral = 0;
	if (firstCalcule_3 == 1) {
		integral = 0;
		derivative = 0;
		old_error_3 = 0;
		firstCalcule_3 = 0;
	}

	error = setpoint - current_position;
	integral = integral + (error * ENCODER_ACQUISITION_TIME_S);
	if(integral >= 9) integral = 9;
	else if(integral < -9) integral = -9;
	derivative = (error - old_error_3) / ENCODER_ACQUISITION_TIME_S;
	output = (kp_3 * error) + (kd_3 * derivative) + (ki_3 * integral);
	if (output > MAX_PID_OUTPUT)
		output = MAX_PID_OUTPUT;
	else if (output < MIN_PID_OUTPUT)
		output = MIN_PID_OUTPUT;
	old_error_3 = error;
	return output;
}

