
#include <Kinematics.h>

void getEffectorPosition(double theta1, double theta2, double theta3, double *buffer){
	double  x, y, z;
	//Convert deg to rad
	double theta1_rad = (theta1*M_PI)/180;
	double theta2_rad = (theta2*M_PI)/180;
	double theta3_rad = (90-theta3)*M_PI/180;
	x = (L3 * cos(theta1_rad) * cos(theta2_rad) * cos(theta3_rad)) - (L3 * cos(theta1_rad) * sin(theta2_rad) * sin(theta3_rad)) + (L2 * cos(theta1_rad) * cos(theta2_rad));
	y = (L3 * cos(theta2_rad) * sin(theta1_rad) * cos(theta3_rad)) - (L3 * sin(theta1_rad) * sin(theta2_rad) * sin(theta3_rad)) + (L2 * cos(theta2_rad) * sin(theta1_rad));
	z = (L3 * cos(theta3_rad) * sin(theta2_rad)) + (L3 * cos(theta2_rad) * sin(theta3_rad)) + (L2*sin(theta2_rad)) + L1;

	buffer[0] = x;
	buffer[1] = y;
	buffer[2] = z;
}

void getArticulationAngles(uint32_t X, uint32_t Y, uint32_t Z, double *buffer){
	float theta1, theta2, theta3;
	Z = Z - L1;												//this is cause the origin of the corrdinates is in the lower base of the robot
	theta1 = atan(Y/X);
	uint32_t r = sqrt((pow(X,2) + pow(Y,2)));

	uint32_t c3 = (pow(Z,2) + pow(Y,2) + pow(X,2) - pow(L2,2) - pow(L3,2)) / (2 * L2 * L3);				//c3 = Cos(theta3)
	uint32_t s3 = sqrt(1 - pow(c3,2));																	//s3 = Sen(theta3)

	theta2 = atan(Z/r) - atan((L3 * s3) / (L2 +L3*(c3)));
	theta3 = 90-(atan(s3 / c3));

	buffer[0] = theta1;
	buffer[1] = theta2;
	buffer[2] = theta3;

}
