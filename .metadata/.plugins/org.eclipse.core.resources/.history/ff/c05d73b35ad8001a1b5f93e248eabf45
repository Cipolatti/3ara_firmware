/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include "DCmotor.h"
#include "IRencoder.h"
#include "Kinematics.h"
#include "usart.h"
#include <stdint.h>
#include "tim.h"
#include <math.h>
#include <stdio.h>
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */


union {
	float floatValue;
	uint8_t bytes[4];
} makeFloat, makeFloat1, makeFloat2, makeBytes1, makeBytes2, makeBytes3, makeBytes4, makeBytes5, makeBytes6;


/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define START_OF_PACKAGE 		0xA1										//Start of the serial package
#define END_OF_PACKAGE 			0xB1										//End of the serial package
#define GO_TO_POSITION 			0x00
#define CHANGE_PID_VARIABLES	0x01
#define GO_TO_ZERO_POSITION 	0x00
#define GO_TO_CERTAIN_POSITION 	0x01
#define READ_ANGLES		 		0x03
#define CHANGE_KINEMATIC_MODEL  0x02										//Command for changing kinematics model
#define READ_POSITION  			0x04										//Command for sending x,y,z position
#define MAX_PID_OUTPUT 			1000						//100% - Max value that can be the duty for the motor (1000)
#define MIN_PID_OUTPUT 			-1000										//0% - Minor value that can be the duty for the motor (-1000)

#define NUMBER_OF_RECEIVED_BYTES	15										//15 bytes are sent from PC to STMF4
#define NUMBER_OF_SENT_BYTES	4
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/*
 * Variables for comms
 */
uint8_t UsartBuffer[NUMBER_OF_RECEIVED_BYTES], dataReceived;

/*
 * Variables for PID
 */
float Ki, Kp, Kd;
float count, output_pid, output_pid_2, output_pid_3, actual_angle_1, actual_angle_2, actual_angle_3,output_pid_1;
uint8_t firstCalcule = 1;
float encoder_1, encoder_2, encoder_3;
/*
 * Variables for kinematics
 */
float position_buffer[3], angle_1, angle_2, angle_3, x_coordinate, y_coordinate, z_coordinate;


/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId CommsHandle;
osThreadId PID1Handle;
osThreadId PID2Handle;
osThreadId PID3Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
float pidCall(float setpoint, float actualposition);
/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartCommsTask(void const * argument);
void StartPID1Task(void const * argument);
void StartPID2task(void const * argument);
void StartPID3task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer,
		StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize) {
	*ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
	*ppxIdleTaskStackBuffer = &xIdleStack[0];
	*pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
	/* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of Comms */
  osThreadDef(Comms, StartCommsTask, osPriorityIdle, 0, 512);
  CommsHandle = osThreadCreate(osThread(Comms), NULL);

  /* definition and creation of PID1 */
  osThreadDef(PID1, StartPID1Task, osPriorityIdle, 0, 512);
  PID1Handle = osThreadCreate(osThread(PID1), NULL);

  /* definition and creation of PID2 */
  osThreadDef(PID2, StartPID2task, osPriorityIdle, 0, 512);
  PID2Handle = osThreadCreate(osThread(PID2), NULL);

  /* definition and creation of PID3 */
  osThreadDef(PID3, StartPID3task, osPriorityIdle, 0, 512);
  PID3Handle = osThreadCreate(osThread(PID3), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
 * @brief  Function implementing the defaultTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */

	/* Infinite loop */
	for (;;) {
		HAL_GPIO_TogglePin(BLUE_LED_GPIO_Port, BLUE_LED_Pin);
		HAL_GPIO_TogglePin(RED_LED_GPIO_Port, RED_LED_Pin);
		osDelay(500);
	}
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartCommsTask */
/**
 * @brief Function implementing the Comms thread.

 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartCommsTask */
void StartCommsTask(void const * argument)
{
  /* USER CODE BEGIN StartCommsTask */
	HAL_UART_Receive_IT(&huart2, (uint8_t*) UsartBuffer, NUMBER_OF_RECEIVED_BYTES);
	uint8_t buffer[] = "HOLA\r\n";
	uint8_t len = sizeof(buffer);
	uint8_t buffer_2[] = "test\r\n";
	uint8_t len_2 = sizeof(buffer_2);
	int count_1 = 0;
	/* Infinite loop */
	for (;;) {

		if (dataReceived == 1) {
			dataReceived = 0;
			if ((UsartBuffer[0] == START_OF_PACKAGE) && (UsartBuffer[14] == END_OF_PACKAGE)) {
				switch (UsartBuffer[1]) {
				case GO_TO_POSITION:
					HAL_UART_Transmit(&huart2, buffer, len ,1000);
					if (UsartBuffer[2] == GO_TO_ZERO_POSITION) {
						//moveToZeroPos(VEL_FOR_ZERO_POSITION);
						HAL_GPIO_TogglePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin);
						osDelay(30);
					} else {

						switch (ChosenKinematics) {
						case FORWARD:
							//positionBuffer[0] = UsartBuffer[2];			//Just for testing
							angle_1 = UsartBuffer[2];//Angle of articulation one
							angle_2 = UsartBuffer[3];//Angle of articulation two
							angle_3 = UsartBuffer[4];//Angle of articulation three
							break;
						case INVERSE:
							x_coordinate = UsartBuffer[2];// X coordinate of the effector
							y_coordinate = UsartBuffer[3];// Y coordinate of the effector
							z_coordinate = UsartBuffer[4];// Z coordinate of the effector
							break;
						}
						osDelay(30);
					}
					break;

				case CHANGE_PID_VARIABLES:
					HAL_UART_Transmit(&huart2, buffer_2, len_2 ,1000);
					HAL_GPIO_TogglePin(ORANGE_LED_GPIO_Port, ORANGE_LED_Pin);
					makeFloat.bytes[0] = UsartBuffer[2];
					makeFloat.bytes[1] = UsartBuffer[3];
					makeFloat.bytes[2] = UsartBuffer[4];
					makeFloat.bytes[3] = UsartBuffer[5];
					Kp = makeFloat.floatValue;		//	Proportional constant
					makeFloat1.bytes[0] = UsartBuffer[6];
					makeFloat1.bytes[1] = UsartBuffer[7];
					makeFloat1.bytes[2] = UsartBuffer[8];
					makeFloat1.bytes[3] = UsartBuffer[9];
					Kd = makeFloat1.floatValue;			// Derivative constant
					makeFloat2.bytes[0] = UsartBuffer[10];
					makeFloat2.bytes[1] = UsartBuffer[11];
					makeFloat2.bytes[2] = UsartBuffer[12];
					makeFloat2.bytes[3] = UsartBuffer[13];
					Ki = makeFloat2.floatValue;			// Integral constantS
					break;

				case CHANGE_KINEMATIC_MODEL:
					if(UsartBuffer[2] == 0x00) ChosenKinematics = INVERSE;
					else if(UsartBuffer[2] == 0x01) ChosenKinematics = FORWARD;
					break;

				case READ_ANGLES:
					{
					/*
					actual_angle_1 = 50.0f;
					actual_angle_2 = 25.0f;
					actual_angle_3 = 2.0f;
					*/
				    makeBytes1.floatValue = actual_angle_1;
				    makeBytes2.floatValue = actual_angle_2;
				    makeBytes3.floatValue = actual_angle_3;
					HAL_GPIO_TogglePin(ORANGE_LED_GPIO_Port, ORANGE_LED_Pin);
					HAL_GPIO_TogglePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin);
					switch (count_1){
						case 0:
							HAL_UART_Transmit(&huart2, makeBytes1.bytes, sizeof(makeBytes1.bytes) ,1000);
							count_1++;
							break;
						case 1:
							HAL_UART_Transmit(&huart2, makeBytes2.bytes, sizeof(makeBytes2.bytes) ,1000);
							count_1++;
							break;
						case 2:
							HAL_UART_Transmit(&huart2, makeBytes3.bytes, sizeof(makeBytes3.bytes) ,1000);
							count_1 = 0;
							break;
						}
					break;
					}

				case READ_POSITION:
				{
					x_coordinate = 5.1f;
					y_coordinate = 5.2f;
					z_coordinate = 5.3f;
				    makeBytes4.floatValue = x_coordinate;
				    makeBytes5.floatValue = y_coordinate;
				    makeBytes6.floatValue = z_coordinate;
					HAL_GPIO_TogglePin(ORANGE_LED_GPIO_Port, ORANGE_LED_Pin);
					HAL_GPIO_TogglePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin);
					switch (count_1){
						case 0:
							HAL_UART_Transmit(&huart2, makeBytes4.bytes, sizeof(makeBytes4.bytes) ,1000);
							count_1++;
							break;
						case 1:
							HAL_UART_Transmit(&huart2, makeBytes5.bytes, sizeof(makeBytes5.bytes) ,1000);
							count_1++;
							break;
						case 2:
							HAL_UART_Transmit(&huart2, makeBytes6.bytes, sizeof(makeBytes6.bytes) ,1000);
							count_1 = 0;
							break;
						}
					break;
				}
				}
			}
		}
		osDelay(100);
	}

  /* USER CODE END StartCommsTask */
}

/* USER CODE BEGIN Header_StartPID1Task */
/**
* @brief Function implementing the PID1 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartPID1Task */
void StartPID1Task(void const * argument)
{
  /* USER CODE BEGIN StartPID1Task */
	//Initializes TIM3  ENCODER
	HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);
	//Initializes TIM2 CH1 PWM
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
	// Initialize duty to zero for motor 1
	htim2.Instance -> CCR2 = 0;
	actual_angle_1 = 0;


  /* Infinite loop */
  for(;;)
  {
	  // Reset encoder count
	  if (HAL_GPIO_ReadPin(END_OF_CAREER_1_GPIO_Port, END_OF_CAREER_1_Pin) == GPIO_PIN_RESET){
		  // Stops the motor
		  htim2.Instance -> CCR2 = 0;
		  // Reset encoder count - Position zero reached
		  __HAL_TIM_SetCounter(&htim4, 0);
	  }

	  switch (ChosenKinematics) {
	  		case FORWARD:
	  			getEffectorPosition(angle_1, angle_2, angle_3, position_buffer);
	  			actual_angle_1 = getAngle1();
	  			output_pid_1 = pidCall(angle_1, actual_angle_1);
	  			if (output_pid_1 > 0) motor1MoveLeft(output_pid_1);
	  			else motor1MoveRight(-output_pid_1);
	  			break;

	  		case INVERSE:
	  			getArticulationAngles(x_coordinate, y_coordinate, z_coordinate, position_buffer);
	  			actual_angle_1 = getAngle1();
	  			output_pid = pidCall(position_buffer[0], actual_angle_1);
	  			if(output_pid > 0) motor1MoveLeft(output_pid);
	  			else motor1MoveRight(-output_pid);
	  			break;
	  		}

	  osDelay(ENCODER_ACQUISITION_TIME);
  }

  /* USER CODE END StartPID1Task */
}

/* USER CODE BEGIN Header_StartPID2task */
/**
* @brief Function implementing the PID2 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartPID2task */
void StartPID2task(void const * argument)
{
  /* USER CODE BEGIN StartPID2task */
	HAL_TIM_Encoder_Start(&htim8, TIM_CHANNEL_ALL);
	//Initializes TIM2 CH1 PWM
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	// Initialize duty to zero for motor 1
	htim2.Instance -> CCR1 = 0;
	actual_angle_2 = 0;


  /* Infinite loop */
  for(;;)
  {
	  // Reset encoder count
	  if (HAL_GPIO_ReadPin(END_OF_CAREER_2_GPIO_Port, END_OF_CAREER_2_Pin) == GPIO_PIN_RESET){
		  // Stops the motor
		  htim2.Instance -> CCR1 = 0;
		  // Reset encoder count - Position zero reached
		  __HAL_TIM_SetCounter(&htim8, 0);
	  }

	  switch (ChosenKinematics) {
	  		case FORWARD:
	  			getEffectorPosition(angle_1, angle_2, angle_3, position_buffer);
	  			actual_angle_2 = getAngle2();
	  			output_pid_2 = pidCall(angle_2, actual_angle_2);
	  			if (output_pid_2 > 0) motor2MoveLeft(output_pid_2);
	  			else motor2MoveRight(-output_pid_2);
	  			break;

	  		case INVERSE:
	  			getArticulationAngles(x_coordinate, y_coordinate, z_coordinate, position_buffer);
	  			actual_angle_2 = getAngle2();
	  			output_pid_2 = pidCall(position_buffer[1], actual_angle_2);
	  			if(output_pid_2 > 0) motor2MoveLeft(output_pid_2);
	  			else motor2MoveRight(-output_pid_2);
	  			break;
	  		}
	  osDelay(ENCODER_ACQUISITION_TIME);
  }
  /* USER CODE END StartPID2task */
}

/* USER CODE BEGIN Header_StartPID3task */
/**
* @brief Function implementing the PID3 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartPID3task */
void StartPID3task(void const * argument)
{
  /* USER CODE BEGIN StartPID3task */
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);						//Initializes TIM  ENCODER
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);							//Initializes TIM2 CH3 PWM
  /* Infinite loop */
  for(;;)
  {
	  if (HAL_GPIO_ReadPin(END_OF_CAREER_3_GPIO_Port, END_OF_CAREER_3_Pin) == GPIO_PIN_RESET){
		  // Stops the motor
		  htim2.Instance -> CCR3 = 0;
		  // Reset encoder count - Position zero reached
		  __HAL_TIM_SetCounter(&htim3, 0);
	  }

	  switch (ChosenKinematics) {
	  		case FORWARD:
	  			actual_angle_3 = getAngle3();
	  			output_pid = pidCall(angle_3, actual_angle_3);
	  			if(output_pid > 0) motor3MoveLeft(output_pid);
	  			else motor3MoveRight(-output_pid);
	  			break;

	  		case INVERSE:
	  			//getArticulationAngles(x_coordinate, y_coordinate, z_coordinate, position_buffer);
	  			actual_angle_3 = getAngle3();
	  			output_pid_3 = pidCall(position_buffer[2], actual_angle_3);
	  			if(output_pid_3 > 0) motor3MoveLeft(output_pid_3);
	  			else motor3MoveRight(-output_pid_3);
	  			break;
	  		}

	  osDelay(ENCODER_ACQUISITION_TIME);
  }
  /* USER CODE END StartPID3task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	if (huart->Instance == USART2) {
		dataReceived = 1;
	}
	HAL_UART_Receive_IT(&huart2, (uint8_t*) UsartBuffer,NUMBER_OF_RECEIVED_BYTES);
}


float pidCall(float setpoint, float actual_position) {
	float error, derivative, integral, output, olderror;
	if (firstCalcule == 1) {
		integral = 0;
		derivative = 0;
		olderror = 0;
		firstCalcule = 0;
	}
	//Ki = 1, Kp = 4, Kd = 1;
	error = setpoint - actual_position;
	integral = integral + (error * ENCODER_ACQUISITION_TIME_S);
	derivative = (error - olderror) / ENCODER_ACQUISITION_TIME_S;
	output = (Kp * error) + (Kd * derivative) + (Ki * integral);
	if (output > MAX_PID_OUTPUT)
		output = MAX_PID_OUTPUT;
	else if (output < MIN_PID_OUTPUT)
		output = MIN_PID_OUTPUT;
	olderror = error;
	return output;
}

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
