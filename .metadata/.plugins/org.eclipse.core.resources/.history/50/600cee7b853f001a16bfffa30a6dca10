/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include "DCmotor.h"
#include "IRencoder.h"
#include "Kinematics.h"
#include "usart.h"
#include "stdint.h"
#include "tim.h"
//#include "IREncoder.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define START_OF_PACKAGE 		0xA1
#define END_OF_PACKAGE 			0xB1
#define GO_TO_POSITION 			0x00
#define CHANGE_PID_VARIABLES	0x01
#define GO_TO_ZERO_POSITION 	0x00
#define GO_TO_CERTAIN_POSITION 	0x01
#define FIXED_MOVEMENT_1 		0x02

#define ENCODER_ACQUISITION_TIME 50
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
uint8_t UsartBuffer[6],dataReceived;
uint8_t Ki, Kp, Kd;										//Variables for PID
uint32_t positionBuffer[3];
uint32_t count;
/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId CommsHandle;
osThreadId MovementHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
   
/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartCommsTask(void const * argument);
void StartMovementTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];
  
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}                   
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of Comms */
  osThreadDef(Comms, StartCommsTask, osPriorityIdle, 0, 128);
  CommsHandle = osThreadCreate(osThread(Comms), NULL);

  /* definition and creation of Movement */
  osThreadDef(Movement, StartMovementTask, osPriorityIdle, 0, 128);
  MovementHandle = osThreadCreate(osThread(Movement), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */

  /* Infinite loop */
  for(;;)
  {
	HAL_GPIO_TogglePin(BLUE_LED_GPIO_Port, BLUE_LED_Pin);
	HAL_GPIO_TogglePin(RED_LED_GPIO_Port, RED_LED_Pin);
    osDelay(250);

  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartCommsTask */
/**
* @brief Function implementing the Comms thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartCommsTask */
void StartCommsTask(void const * argument)
{
  /* USER CODE BEGIN StartCommsTask */
	HAL_UART_Receive_IT(&huart1, (uint8_t *) UsartBuffer, 6);

  /* Infinite loop */
  for(;;)
  {
	  if(dataReceived == 1){
		  dataReceived = 0;
			if((UsartBuffer[0] == START_OF_PACKAGE) && (UsartBuffer[5] == END_OF_PACKAGE)){
				switch(UsartBuffer[1]){
					case GO_TO_POSITION:
						if(UsartBuffer[2] == GO_TO_ZERO_POSITION){
							//MoveToZeroPos();
							HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_SET);
							HAL_GPIO_WritePin(ORANGE_LED_GPIO_Port, ORANGE_LED_Pin, GPIO_PIN_RESET);
							osDelay(30);
						}
						else if(UsartBuffer[2] == GO_TO_CERTAIN_POSITION){
							HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_RESET);
							HAL_GPIO_WritePin(ORANGE_LED_GPIO_Port, ORANGE_LED_Pin, GPIO_PIN_SET);
							osDelay(30);
						}
						break;

					case CHANGE_PID_VARIABLES:
							Kp = UsartBuffer[2];
							Kd = UsartBuffer[3];
							Ki = UsartBuffer[4];
						break;
					case FIXED_MOVEMENT_1:

						break;
				}
			}
	  }
    osDelay(10);
  }

  /* USER CODE END StartCommsTask */
}

/* USER CODE BEGIN Header_StartMovementTask */
/**
* @brief Function implementing the Movement thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartMovementTask */
void StartMovementTask(void const * argument)
{
  /* USER CODE BEGIN StartMovementTask */

	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&htim8, TIM_CHANNEL_ALL);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
	htim2.Instance -> CCR1 = 80;

	//MoveToZeroPos();
  /* Infinite loop */
  for(;;)
  {

	  count = GetRPM(2);
	  if(count >= 5){
		  count =0;
		  HAL_GPIO_WritePin(ORANGE_LED_GPIO_Port, ORANGE_LED_Pin, GPIO_PIN_SET);
		  osDelay(250);
		  HAL_GPIO_WritePin(ORANGE_LED_GPIO_Port, ORANGE_LED_Pin, GPIO_PIN_RESET);
		  osDelay(250);
	  }
	  osDelay(ENCODER_ACQUISITION_TIME);
  }
  /* USER CODE END StartMovementTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if (huart->Instance == USART1){
		dataReceived = 1;
	}
	HAL_UART_Receive_IT(&huart1, (uint8_t *) UsartBuffer, 6);
}

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
